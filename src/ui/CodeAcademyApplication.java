package ui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/* 
 * @desc starts the program
 */
public class CodeAcademyApplication extends Application {
    public static void main(String[] args) {
        launch(CodeAcademyApplication.class);
    }

    @Override
    public void start(Stage window) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("views/home-overview.fxml"));
            window.setTitle("CodeCademy");
            window.setScene(new Scene(root, 1280, 800));
            window.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}