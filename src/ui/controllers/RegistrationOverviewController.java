package ui.controllers;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import domain.Registration;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import logic.RegistrationService;

public class RegistrationOverviewController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private GridPane registrationGrid;
    

    // gets and sets all the data for the registration overview window
    @FXML
    void initialize() {
        RegistrationService service = new RegistrationService();

        try {
            List<Registration> registrations = service.getAllRegistrations();

            int column = 0;
            int row = 1;

            for (int i = 0; i < registrations.size(); i++) {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("../views/registration-preview.fxml"));
                AnchorPane anchorPane = fxmlLoader.load();

                RegistrationPreview previewController = fxmlLoader.getController();
                Registration registration = registrations.get(i);
                previewController.setData(registration);

                if (column == 3) {
                    column = 0;
                    row++;
                }

                anchorPane.setOnMouseClicked(new EventHandler<Event>() {
                    @Override
                    public void handle(Event event) {

                    }
                });

                registrationGrid.add(anchorPane, column, row);
                column++;
                
                registrationGrid.setMinWidth(Region.USE_COMPUTED_SIZE);
                registrationGrid.setPrefWidth(Region.USE_COMPUTED_SIZE);
                registrationGrid.setMaxWidth(Region.USE_PREF_SIZE);
    
                registrationGrid.setMinHeight(Region.USE_COMPUTED_SIZE);
                registrationGrid.setPrefHeight(Region.USE_COMPUTED_SIZE);
                registrationGrid.setMaxHeight(Region.USE_PREF_SIZE);
    
                GridPane.setMargin(anchorPane, new Insets(10));
            }
            registrationGrid.setPadding(new Insets(-30, 0, 0, 0));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // opens a window to add a registration
    @FXML
    public void addRegistration() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../views/registration-add.fxml"));
            Parent root = fxmlLoader.load();
            Stage stage = new Stage();
            stage.setTitle("Add new registration");
            stage.setScene(new Scene(root, 450, 300));
            stage.showAndWait();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
