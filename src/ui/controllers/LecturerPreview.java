package ui.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import domain.Lecturer;
import javafx.fxml.FXML;
import javafx.scene.text.Text;

public class LecturerPreview {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Text subjectAndDifficulty;

    @FXML
    private Text introText;

    @FXML
    private Text name;

    private Lecturer lecturer;

    @FXML
    void initialize() {

    }
    // sets the data for the lecturer
    public void setData(Lecturer lecturer) {
        this.lecturer = lecturer;
        this.name.setText(this.lecturer.getName());
        this.subjectAndDifficulty.setText(this.lecturer.getEmail());
        this.introText.setText(this.lecturer.getOrganisation());
    }
}
