package ui.controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import domain.Course;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import logic.CourseService;
import logic.ModuleService;
import domain.Module;

public class CourseDetailsController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Text introText;

    @FXML
    private Text name;

    @FXML
    private Text totalPassed;

    @FXML
    private Text subjectAndDifficulty;

    @FXML
    private GridPane moduleGrid;

    @FXML
    private GridPane courseGrid;

    private CourseService courseService = new CourseService();
    private ModuleService moduleService = new ModuleService();

    private Stage stage;
    private Scene scene;
    private Course course;

    @FXML
    void initialize() {

    }
    // set all the data given from its parent in this window, fill the placeholders with data
    public void setData(Course course) {
        try {
            this.course = this.courseService.getCourse(course);
            this.name.setText(course.getName());
            this.subjectAndDifficulty.setText(course.getSubject() + " - " + course.getDifficulty());
            this.introText.setText(course.getIntroduction());
            this.totalPassed.setText("Total passed: " + String.valueOf(this.course.getPassedCount()));
            this.initializeModuleGrid();
            this.initializeRelatedCoursesGrid();
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../views/error/sql-error.fxml"));
                Parent root;
                root = fxmlLoader.load();
                SQLErrorController sqlErrorController = fxmlLoader.getController();
                sqlErrorController.setData(e.getMessage());
    
                Stage stage = new Stage();
                stage.setScene(new Scene(root, 446, 245));
                stage.showAndWait();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
    // Function that opens the update course window
    public void showUpdateCourse() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../views/course-update.fxml"));
            Parent root = fxmlLoader.load();

            CourseUpdateController courseUpdateController = fxmlLoader.getController();
            courseUpdateController.setData(course);

            Stage stage = new Stage();
            stage.setTitle("Update course");
            stage.setScene(new Scene(root, 450, 380));
            stage.showAndWait();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    // function that deletes selected course
    public void deleteCourse(ActionEvent event) {
        try {
            // Delete course
            courseService.deleteCourse(this.course);

            // Switch back to course overview
            Parent root = FXMLLoader.load(getClass().getResource("../views/course-overview.fxml"));
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            Scene scene = new Scene(root);
            stage.setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Shows the average progress of each module in gridpane
    private void initializeModuleGrid() throws SQLException {
        List<Module> modules = this.moduleService.getModules(this.course.getName());

        Collections.sort(modules, (m1, m2) -> m1.getIndex() - m2.getIndex());

        for (int i = 0; i < modules.size(); i++) {
            Module module = modules.get(i);
            Label label = new Label(String.format("%o. %s %.1f - %s - Average progress: %.2f%%", module.getIndex(), module.getTitle(), module.getVersion(), module.getPublicationDate().format(DateTimeFormatter.ofPattern("dd-MM-yy")), module.getAverageProgression()));
            label.setFont(new Font("Arial", 14));
            this.moduleGrid.add(label, 0, i + 1);
        }

        this.moduleGrid.setPadding(new Insets(-7, 0, 0, 5));
    }

    //Fills relatedCourse gridpane with related courses
    private void initializeRelatedCoursesGrid() throws SQLException {

        try {
            List<Course> courses = this.courseService.getRelatedCourses(this.course);

            for (int i = 0; i < courses.size(); i++) {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("../views/course-preview.fxml"));
                AnchorPane anchorPane = fxmlLoader.load();
    
                CoursePreview previewController = fxmlLoader.getController();
                Course course = courses.get(i);
                previewController.setData(course);
                
                anchorPane.setOnMouseClicked(new EventHandler<Event>() {
                    @Override
                    public void handle(Event event) {
                        try {
                            FXMLLoader fxmlLoader = new FXMLLoader();
                            fxmlLoader.setLocation(getClass().getResource("../views/course-details.fxml"));
                            Parent root = fxmlLoader.load();
                            CourseDetailsController detailsController = fxmlLoader.getController();
                            detailsController.setData(course);
                            stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
                            scene = new Scene(root);
                            stage.setScene(scene);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                
                courseGrid.add(anchorPane, i, 1);
                
                courseGrid.setMinWidth(Region.USE_COMPUTED_SIZE);
                courseGrid.setPrefWidth(Region.USE_COMPUTED_SIZE);
                courseGrid.setMaxWidth(Region.USE_PREF_SIZE);
    
                courseGrid.setMinHeight(Region.USE_COMPUTED_SIZE);
                courseGrid.setPrefHeight(Region.USE_COMPUTED_SIZE);
                courseGrid.setMaxHeight(Region.USE_PREF_SIZE);
    
                GridPane.setMargin(anchorPane, new Insets(20));
            }
            courseGrid.setPadding(new Insets(-30, 0, 0, 0));
        } catch (Exception e) { 
            e.printStackTrace();
        }
        this.moduleGrid.setPadding(new Insets(-7, 0, 0, 5));
    }
}
