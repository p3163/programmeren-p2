package ui.controllers;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/*
 * Controls the actions of the nav buttons inside the navbar
 */
public class NavController {
    private Stage stage;
    private Scene scene;

    /*
     * @desc switches to the home-overview view
     */
    public void switchToHomeOverview(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../views/home-overview.fxml"));
        stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
    }

    /*
     * @desc switches to the course-overview view
     */
    public void switchToCourseOverview(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../views/course-overview.fxml"));
        stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
    }

    /*
     * @desc switches to the certificate-overview view
     */
    public void switchToCertificateOverview(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../views/certificate/certificate-overview.fxml"));
        stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
    }

    /*
     * @desc switches to the registrations-overview view
     */
    public void  switchToRegistrationOverview(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../views/registration-overview.fxml"));
        stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
    }

    /*
     * @desc switches to the user-overview view
     */
    public void switchToUserOverview(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../views/user/user-overview.fxml"));
        stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
    }
}
