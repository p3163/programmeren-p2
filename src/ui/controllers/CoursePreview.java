package ui.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import domain.Course;
import javafx.fxml.FXML;
import javafx.scene.text.Text;

public class CoursePreview {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Text subjectAndDifficulty;

    @FXML
    private Text introText;

    @FXML
    private Text name;

    private Course course;

    @FXML
    void initialize() {

    }
    // sets course data for the tile
    public void setData(Course course) {
        this.course = course;
        this.name.setText(this.course.getName());
        this.subjectAndDifficulty.setText(this.course.getSubject() + " - " + this.course.getDifficulty());
        this.introText.setText(this.course.getIntroduction());
    }
}
