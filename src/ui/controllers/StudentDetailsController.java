package ui.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import domain.Certificate;
import domain.Registration;
import domain.Student;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import logic.CertificateService;
import logic.RegistrationService;
import logic.StudentService;

public class StudentDetailsController {

    @FXML
    private Text city;

    @FXML
    private Text dateofbirth;

    @FXML
    private Button editStudent;

    @FXML
    private Text gender;

    @FXML
    private Text name;

    @FXML
    private GridPane completedCourses;

    private StudentService service = new StudentService();
    private CertificateService certificateService = new CertificateService();
    private RegistrationService registrationService = new RegistrationService();

    private Student student;

    @FXML
    void initialize() {

    }
    // sets data for student
    public void setData(Student value) {
        try {
            Student student = this.service.getStudent(value);
            this.name.setText(student.getName());
            this.gender.setText(student.getGender());
            this.city.setText(student.getCity());
            this.dateofbirth.setText(student.getDateOfBirth());
            this.student = value;
            this.initializeCompletedCourse(student);
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../views/error/sql-error.fxml"));
                Parent root;
                root = fxmlLoader.load();
                SQLErrorController sqlErrorController = fxmlLoader.getController();
                sqlErrorController.setData(e.getMessage());
    
                Stage stage = new Stage();
                stage.setScene(new Scene(root, 446, 245));
                stage.showAndWait();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
    // opens edit student window
    public void editStudent(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../views/user/student-update.fxml"));
            Parent root = fxmlLoader.load();

            StudentUpdateController studentUpdatecontroller = fxmlLoader.getController();
            studentUpdatecontroller.setData(student);

            Stage stage = new Stage();
            stage.setTitle("Update Student");
            stage.setScene(new Scene(root, 450, 380));
            stage.showAndWait();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    // attempts to delete selected student
    public void deleteStudent(ActionEvent event) {
        try {
            //Delete student
            service.deleteStudent(this.student);

            //Switch back to user overview
            Parent root;
            root = FXMLLoader.load(getClass().getResource("../views/user/user-overview.fxml"));
            Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            Scene scene = new Scene(root);
            stage.setScene(scene);
        }  catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Sets the completed courses for each student
    private void initializeCompletedCourse(Student student) throws SQLException {
        List<Registration> registrations = 
            registrationService.getAllRegistrations()
                .stream().filter(x -> x.getStudent().equals(student.getEmail()))
                .collect(Collectors.toList());

        List<Certificate> certificates = new ArrayList<>();

        for (Registration registration : registrations) {
            certificates.add(
                certificateService
                .getAllCertificates()
                .stream()
                .filter(x -> x.getID() == registration.getCertificateId())
                .findFirst()
                .orElse(null));
        }

        for (int i = 0; i < certificates.size(); i++) {
            Certificate certificate = certificates.get(i);
            Registration registration = registrations.stream().filter(x -> x.getCertificateId() == certificate.getID()).findFirst().orElse(null);
            Label label = new Label(String.format("Completed course: %s", registration.getCourse()));
            label.setAlignment(Pos.TOP_LEFT);
            label.setFont(new Font("Arial", 14));
            GridPane.setHalignment(label, HPos.LEFT);
            this.completedCourses.add(label, 0, i + 1);
        }

        this.completedCourses.setPadding(new Insets(-7, 0, 0, 5));
    }
}