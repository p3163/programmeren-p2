package ui.controllers;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import domain.Registration;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import logic.CertificateService;
import logic.RegistrationService;

public class CertificateEditController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField expert;

    @FXML
    private TextField grade;

    @FXML
    private ComboBox<String> registrations;

    private CertificateService certificateService = new CertificateService();
    private RegistrationService registrationService = new RegistrationService();

    // Grab Certificate to edit, on initialize
    @FXML
    void initialize() {
        try {
            this.registrations.setItems(
                    FXCollections.observableArrayList(
                            registrationService.getAllAvailableRegistrations().stream().map(
                                    registration -> {
                                        return registration.toString();
                                    }).collect(Collectors.toList())));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Save certificate function
    public void save(ActionEvent event) {
        String[] parts = registrations.getValue().split(" - ");
        Registration registration = new Registration(parts[0], parts[1], LocalDate.parse(parts[2]));
        try {
            certificateService.createCertificate(Double.valueOf(this.grade.getText()), this.expert.getText(), registration);
            this.close(event);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Close the edit certificate window
    public void close(ActionEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.close();
    }
}
