package ui.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import domain.Registration;
import javafx.fxml.FXML;
import javafx.scene.text.Text;

public class RegistrationPreview {
    
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Text courseName;

    @FXML
    private Text name;

    @FXML
    private Text registrationDate;

    @FXML
    private Text studentEmail;

    @FXML
    void initialize() {
    }
    // sets data for the registration
    public void setData(Registration registration) {
        this.courseName.setText(registration.getCourse());
        this.studentEmail.setText(registration.getStudent());
        this.registrationDate.setText(registration.getRegistrationDate().toString());
    }
}
