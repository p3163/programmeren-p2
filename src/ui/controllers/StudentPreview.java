package ui.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import domain.Student;
import javafx.fxml.FXML;
import javafx.scene.text.Text;

public class StudentPreview {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Text subjectAndDifficulty;

    @FXML
    private Text introText;

    @FXML
    private Text name;

    private Student student;

    @FXML
    void initialize() {

    }
    // sets the students data
    public void setData(Student student) {
        this.student = student;
        this.name.setText(this.student.getName());
        this.subjectAndDifficulty.setText(this.student.getEmail());
        this.introText.setText(this.student.getGender());
    }
}
