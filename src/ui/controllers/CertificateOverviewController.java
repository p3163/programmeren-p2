package ui.controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.stream.Collectors;


import domain.Certificate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import logic.CertificateService;

public class CertificateOverviewController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TableView<Certificate> table;

    @FXML
    private TableColumn<Certificate, Double> colGrade;

    @FXML
    private TableColumn<Certificate, String> coldExpertName;

    private CertificateService certificateService = new CertificateService();

    // Grab all certificates and set in table
    @FXML
    void initialize() {
        try {
            this.colGrade.setCellValueFactory(new PropertyValueFactory<Certificate, Double>("grade"));
            this.coldExpertName.setCellValueFactory(new PropertyValueFactory<Certificate, String>("nameOfExpert"));
            this.table.setItems(this.getCertificates());

            table.setRowFactory(tv -> {
                TableRow<Certificate> row = new TableRow<>();
                row.setOnMouseClicked(event -> {
                    if (!row.isEmpty() && event.getButton()==MouseButton.PRIMARY 
                         && event.getClickCount() == 2) {
                        this.showCertificateUpdate(row.getItem(), event);
                    }
                });
                return row ;
            });

        } catch (SQLException e) {
            e.printStackTrace();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../views/error/sql-error.fxml"));
                Parent root;
                root = fxmlLoader.load();
                SQLErrorController sqlErrorController = fxmlLoader.getController();
                sqlErrorController.setData(e.getMessage());
    
                Stage stage = new Stage();
                stage.setScene(new Scene(root, 446, 245));
                stage.showAndWait();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        } 
    }
    // Open the certificte edit window
    public void showCertificateEdit() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../views/certificate/certificate-edit.fxml"));
            Parent root = fxmlLoader.load();
            Stage stage = new Stage();
            stage.setTitle("Add new certificate");
            stage.setScene(new Scene(root, 450, 300));
            stage.showAndWait();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // OPen certificate update window
    public void showCertificateUpdate(Certificate certificate, MouseEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../views/certificate/certificate-update.fxml"));
            Parent root = fxmlLoader.load();
            CertificateUpdateController controller = fxmlLoader.getController();
            controller.setData(certificate);
            Stage stage = new Stage();
            stage.setTitle("Add new certificate");
            stage.setScene(new Scene(root, 450, 250));
            stage.showAndWait();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // function to get all the certificates
    private ObservableList<Certificate> getCertificates() throws SQLException {
        ObservableList<Certificate> colValues = FXCollections.observableArrayList(
            this.certificateService.getAllCertificates().stream().map(
                    certificate -> {
                        return certificate;
                    }).collect(Collectors.toList()));

        return colValues;
    }
}
