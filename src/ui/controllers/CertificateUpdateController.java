package ui.controllers;

import domain.Certificate;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import logic.CertificateService;

public class CertificateUpdateController {

    @FXML
    private TextField expert;

    @FXML
    private TextField grade;

    private Certificate certificate;

    private CertificateService certificateService = new CertificateService();

    @FXML
    void initialize() {

    }

    // save the certificate
    public void save(ActionEvent event) {
        try {
            this.certificateService.updateCertificate(this.certificate, Double.valueOf(this.grade.getText()), this.expert.getText());
            this.close(event);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // delete the certificate
    public void delete(ActionEvent event) {
        try {
            this.certificateService.deleteCertificate(this.certificate);
            this.close(event);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // close the certificate update window
    public void close(ActionEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.close();
    }
    // function to set the data of the certificate in this window
    public void setData(Certificate certificate) {
        this.certificate = certificate;
        this.expert.setText(certificate.getNameOfExpert());
        this.grade.setText(String.valueOf(certificate.getGrade()));
    }
}
