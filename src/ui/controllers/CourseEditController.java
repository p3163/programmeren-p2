package ui.controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import logic.CourseService;
import logic.ModuleService;
import domain.Module;

public class CourseEditController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private MenuButton difficulty;

    @FXML
    private TextArea introText;

    @FXML
    private TextField subject;

    @FXML
    private TextField name;

    @FXML
    private GridPane moduleList;

    private ModuleService moduleService = new ModuleService();
    // call function upon initializing that initilizes a list in this window
    @FXML
    void initialize() {
        try {
            List<Module> modules = this.moduleService.getAllAvailableModules();
            this.initializeCheckboxList(modules);
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../views/error/sql-error.fxml"));
                Parent root;
                root = fxmlLoader.load();
                SQLErrorController sqlErrorController = fxmlLoader.getController();
                sqlErrorController.setData(e.getMessage());
    
                Stage stage = new Stage();
                stage.setScene(new Scene(root, 446, 245));
                stage.showAndWait();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
    // save course
    public void save(ActionEvent event) {
        CourseService courseService = new CourseService();
        try {
            courseService.createCourse(name.getText(), introText.getText(), subject.getText(), difficulty.getText());
            List<String> titles = this.getCheckedTitles();
            if (titles.size() > 0) {
                this.moduleService.updateAllModules(name.getText(), titles);
            }
            this.close(event);
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../views/error/sql-error.fxml"));
                Parent root;
                root = fxmlLoader.load();
                SQLErrorController sqlErrorController = fxmlLoader.getController();
                sqlErrorController.setData(e.getMessage());
    
                Stage stage = new Stage();
                stage.setScene(new Scene(root, 446, 245));
                stage.showAndWait();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
    // close the window
    public void close(ActionEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.close();
    }
    // Change the difficulty menu button
    public void changeMenuButtonValue(ActionEvent event) {
        MenuItem item = (MenuItem) event.getSource();
        String temp = difficulty.getText();
        difficulty.setText(item.getText());
        item.setText(temp);
    }
    // add modules
    private void initializeCheckboxList(List<Module> modules) {
        for (int i = 0; i < modules.size(); i++) {
            moduleList.add(new CheckBox(modules.get(i).getTitle()), 0, i + 1);
        }

        moduleList.setPadding(new Insets(-7, 0, 0, 5));
    }
    // gets all the titles
    private List<String> getCheckedTitles() {
        List<String> titles = new ArrayList<>();

        for (Node child : moduleList.getChildren()) {
            CheckBox box = (CheckBox) child;
            if (box.isSelected()) {
                titles.add(box.getText());
            }
        }
        return titles;
    }
}
