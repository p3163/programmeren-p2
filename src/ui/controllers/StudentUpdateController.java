package ui.controllers;

import java.io.IOException;
import java.sql.SQLException;

import domain.Student;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import logic.StudentService;

public class StudentUpdateController {

    @FXML
    private TextField addendom;

    @FXML
    private TextField city;

    @FXML
    private TextField country;

    @FXML
    private TextField dateofbirth;

    @FXML
    private TextField email;

    @FXML
    private SplitMenuButton gender;

    @FXML
    private TextField housenumber;

    @FXML
    private TextField name;

    @FXML
    private TextField street;

    @FXML
    private TextField zipcode;

    @FXML
    void changeMenuButtonValue(ActionEvent event) {

    }
    // sets the data for the student to update
    public void setData(Student student) {
        this.name.setText(student.getName());
        this.email.setText(student.getEmail());
        this.city.setText(student.getCity());
        this.dateofbirth.setText(student.getDateOfBirth());
        this.street.setText(student.getStreet());
        this.zipcode.setText(student.getZipcode());
        this.gender.setText(student.getGender());
        this.addendom.setText(student.getHouseNumberAddendom());
        this.housenumber.setText(String.valueOf(student.getHouseNumber()));
        this.country.setText(String.valueOf(student.getCountry()));
    }
    // closes window
    @FXML
    void close(ActionEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.close();
    }
    // attempts to save changes
    @FXML
    void save(ActionEvent event) {
        StudentService service = new StudentService();
        try {
            service.updateStudent(name.getText(), email.getText(), dateofbirth.getText(), gender.getText(), city.getText(), country.getText(), street.getText(), Integer.valueOf(housenumber.getText()), addendom.getText(), zipcode.getText());
            this.close(event);
        } catch (SQLException | IllegalArgumentException e) {
            e.printStackTrace();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../views/error/sql-error.fxml"));
                Parent root;
                root = fxmlLoader.load();
                SQLErrorController sqlErrorController = fxmlLoader.getController();
                sqlErrorController.setData(e.getMessage());
    
                Stage stage = new Stage();
                stage.setScene(new Scene(root, 446, 245));
                stage.showAndWait();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

}
