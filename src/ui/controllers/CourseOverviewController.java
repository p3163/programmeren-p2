package ui.controllers;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import domain.Course;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import logic.CourseService;
import javafx.scene.Node;

public class CourseOverviewController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private SplitMenuButton dropdown;

    @FXML
    private GridPane courseGrid;
    
    private Stage stage;
    private Scene scene;

    private String filter = "A-Z";

    // gets & sets all the courses and applies current filter setting
    @FXML
    void initialize() {
        CourseService service = new CourseService();

        try {
            List<Course> courses;
            if(filter.equals("A-Z")) {
                courses = service.getAllCourses();
            } else {
                courses = service.getPopularCourses();
            }

            int column = 0;
            int row = 1;

            for (int i = 0; i < courses.size(); i++) {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("../views/course-preview.fxml"));
                AnchorPane anchorPane = fxmlLoader.load();
    
                CoursePreview previewController = fxmlLoader.getController();
                Course course = courses.get(i);
                previewController.setData(course);

                if (column == 3) {
                    column = 0;
                    row++;
                }

                anchorPane.setOnMouseClicked(new EventHandler<Event>() {
                    @Override
                    public void handle(Event event) {
                        try {
                            FXMLLoader fxmlLoader = new FXMLLoader();
                            fxmlLoader.setLocation(getClass().getResource("../views/course-details.fxml"));
                            Parent root = fxmlLoader.load();
                            CourseDetailsController detailsController = fxmlLoader.getController();
                            detailsController.setData(course);
                            stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
                            scene = new Scene(root);
                            stage.setScene(scene);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                
                courseGrid.add(anchorPane, column, row);
                column++;
                
                courseGrid.setMinWidth(Region.USE_COMPUTED_SIZE);
                courseGrid.setPrefWidth(Region.USE_COMPUTED_SIZE);
                courseGrid.setMaxWidth(Region.USE_PREF_SIZE);
    
                courseGrid.setMinHeight(Region.USE_COMPUTED_SIZE);
                courseGrid.setPrefHeight(Region.USE_COMPUTED_SIZE);
                courseGrid.setMaxHeight(Region.USE_PREF_SIZE);
    
                GridPane.setMargin(anchorPane, new Insets(10));
            }
            courseGrid.setPadding(new Insets(-30, 0, 0, 0));
        } catch (Exception e) { 
            e.printStackTrace();
        }
    }
    // Changes the courses overview filter
    @FXML
    void changeMenuButtonValue(ActionEvent event) {
        MenuItem item = (MenuItem) event.getSource();
        if(!item.getText().equals(filter)) {
            dropdown.setText(item.getText());
            filter = item.getText();
            courseGrid.getChildren().clear();
            initialize();
        }
    }
    // shows the course edit window upon clicking button
    public void showCourseEdit() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../views/course-edit.fxml"));
            Parent root = fxmlLoader.load();
            Stage stage = new Stage();
            stage.setTitle("Add new course");
            stage.setScene(new Scene(root, 450, 525));
            stage.showAndWait();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
