package ui.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import domain.Student;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import logic.StudentService;
import javafx.scene.Node;

public class UserOverviewController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private GridPane userGrid;
    
    private Stage stage;
    private Scene scene;

    // upon opening get all students and set data in tiled grid
    @FXML
    void initialize() {
        StudentService service = new StudentService();

        try {
            List<Student> students = service.getAllStudents();

            int column = 0;
            int row = 1;

            for (int i = 0; i < students.size(); i++) {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("../views/user/student-preview.fxml"));
                AnchorPane anchorPane = fxmlLoader.load();
    
                StudentPreview previewController = fxmlLoader.getController();
                Student student = students.get(i);
                previewController.setData(student);

                if (column == 3) {
                    column = 0;
                    row++;
                }

                anchorPane.setOnMouseClicked(new EventHandler<Event>() {
                    @Override
                    public void handle(Event event) {
                        try {
                            FXMLLoader fxmlLoader = new FXMLLoader();
                            fxmlLoader.setLocation(getClass().getResource("../views/user/student-details.fxml"));
                            Parent root = fxmlLoader.load();
                            StudentDetailsController detailsController = fxmlLoader.getController();
                            detailsController.setData(student);
                            stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
                            scene = new Scene(root);
                            stage.setScene(scene);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                
                userGrid.add(anchorPane, column, row);
                column++;
                
                userGrid.setMinWidth(Region.USE_COMPUTED_SIZE);
                userGrid.setPrefWidth(Region.USE_COMPUTED_SIZE);
                userGrid.setMaxWidth(Region.USE_PREF_SIZE);
    
                userGrid.setMinHeight(Region.USE_COMPUTED_SIZE);
                userGrid.setPrefHeight(Region.USE_COMPUTED_SIZE);
                userGrid.setMaxHeight(Region.USE_PREF_SIZE);
    
                GridPane.setMargin(anchorPane, new Insets(10));
            }
            userGrid.setPadding(new Insets(-30, 0, 0, 0));
        } catch (Exception e) {
            e.printStackTrace();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../views/error/sql-error.fxml"));
                Parent root;
                root = fxmlLoader.load();
                SQLErrorController sqlErrorController = fxmlLoader.getController();
                sqlErrorController.setData(e.getMessage());
    
                Stage stage = new Stage();
                stage.setScene(new Scene(root, 446, 245));
                stage.showAndWait();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
    // upon pressing button, fires function and opens student menu
    public void showAddStudentMenu() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../views/user/student-create.fxml"));
            Parent root = fxmlLoader.load();
            Stage stage = new Stage();
            stage.setTitle("Add new Student");
            stage.setScene(new Scene(root, 450, 550));
            stage.showAndWait();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
