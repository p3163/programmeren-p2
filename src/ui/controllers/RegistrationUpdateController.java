package ui.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import logic.CourseService;
import logic.RegistrationService;
import logic.StudentService;

public class RegistrationUpdateController implements Initializable {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ComboBox<String> students;

    @FXML
    private ComboBox<String> courses;

    @FXML
    private TextField day;

    @FXML
    private TextField month;

    @FXML
    private TextField year;

    private StudentService studentService = new StudentService();
    private CourseService courseService = new CourseService();
    private RegistrationService registrationService = new RegistrationService();
    // gets all the emails and course names for the combo box upon initialize
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            students.setItems(
                    FXCollections.observableArrayList(
                            this.studentService.getAllStudents().stream().map(
                                    student -> {
                                        return student.getEmail();
                                    }).collect(Collectors.toList())));

            courses.setItems(
                    FXCollections.observableArrayList(
                            this.courseService.getAllCourses().stream().map(
                                    course -> {
                                        return course.getName();
                                    }).collect(Collectors.toList())));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //  closes window
    @FXML
    void close(ActionEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.close();
    }
    // attempts to save the updated registration
    @FXML
    void save(ActionEvent event) {
        try {
            registrationService.createRegistration(this.students.getValue(), this.courses.getValue(), Integer.valueOf(this.day.getText()), Integer.valueOf(this.month.getText()), Integer.valueOf(this.year.getText()));
            this.close(event); 
        } catch (Exception e) {
            e.printStackTrace();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../views/error/sql-error.fxml"));
                Parent root;
                root = fxmlLoader.load();
                SQLErrorController sqlErrorController = fxmlLoader.getController();
                sqlErrorController.setData(e.getMessage());
    
                Stage stage = new Stage();
                stage.setScene(new Scene(root, 446, 245));
                stage.showAndWait();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
}
