package ui.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import domain.Course;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import logic.CourseService;

public class CourseUpdateController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private SplitMenuButton difficulty;

    @FXML
    private TextArea introText;

    @FXML
    private TextField name;

    @FXML
    private TextField subject;

    Course course = null;

    CourseService courseService = new CourseService();

    @FXML
    void initialize() {

    }
    // sets the course data in the edit window
    public void setData(Course course) {
        this.name.setText(course.getName());
        this.name.setDisable(true);
        this.subject.setText(course.getSubject());
        this.difficulty.setText(course.getDifficulty());
        this.introText.setText(course.getIntroduction());
        this.course = course;
    }
    // attempts to save the data upon editing
    public void save(ActionEvent event) {
        try {
            this.course.setIntroduction(this.introText.getText());
            this.course.setSubject(this.subject.getText());
            this.course.setDifficulty(this.difficulty.getText());
            this.courseService.updateCourse(this.course);
            this.close(event);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../views/error/sql-error.fxml"));
                Parent root;
                root = fxmlLoader.load();
                SQLErrorController sqlErrorController = fxmlLoader.getController();
                sqlErrorController.setData(e.getMessage());

                Stage stage = new Stage();
                stage.setScene(new Scene(root, 450, 380));
                stage.showAndWait();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            
        }
    }
    // closes window
    public void close(ActionEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.close();
    }
    // changes value upon selecting a different value in the selector
    public void changeMenuButtonValue(ActionEvent event) {
        MenuItem item = (MenuItem) event.getSource();
        String temp = difficulty.getText();
        difficulty.setText(item.getText());
        item.setText(temp);
    }
}
