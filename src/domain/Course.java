package domain;

import java.util.ArrayList;
import java.util.List;

/**
 * @desc course domain class
 */
public class Course {
    private String name;
    private String introduction;
    private String subject;
    private String difficulty;
    private List<Registration> registrations;

    public Course(String name, String introduction, String subject, String difficulty) {
        this.name = name;
        this.introduction = introduction;
        this.subject = subject;
        this.difficulty = difficulty;
        this.registrations = new ArrayList<>();
    }

    public Course(String name, String introduction, String subject, String difficulty, List<Registration> registrations) {
        this(name, introduction, subject, difficulty);
        this.registrations = registrations;
    }
    
    //ophalen van Naam
    public String getName() {
        return this.name;
    }

    //setten van Naam
    public void setName(String name) {
        this.name = name;
    }

    //ophalen van introductie
    public String getIntroduction() {
        return this.introduction;
    }

    //setten van Naam
    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    //ophalen van vak
    public String getSubject() {
        return this.subject;
    }

    //setten van Naam
    public void setSubject(String subject) {
        this.subject = subject;
    }

    //ophalen van moeilijkheidsgraad
    public String getDifficulty() {
        return this.difficulty;
    }

    //setten van Naam
    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    //ophalen van registraties
    public List<Registration> getRegistrations() {
        return this.registrations;
    }

    //setten van Naam
    public void setRegistrations(List<Registration> registrations) {
        this.registrations = registrations;
    }

    //registratie toevoegen
    public void addRegistration(Registration registration) {
        this.registrations.add(registration);
    }

    //ophalen van hoeveelheid gehaalde studenten
    public int getPassedCount() {
        return (int) this.registrations.stream().filter(r -> r.getCertificateId() != 0).count();
    }
}
