package domain;

import java.time.LocalDate;

public class Module extends ContentItem {
    private double version;
    private int index;

    public Module(int id, LocalDate publicationDate, String title, String description, double version, int index) {
        super(id, publicationDate, title, description);
        this.version = version;
        this.index = index;
    }

    //ophalen van Versie
    public double getVersion() {
        return this.version;
    }

    //setten van Versie
    public void setVersion(double version) {
        this.version = version;
    }

    //ophalen van Index
    public int getIndex() {
        return this.index;
    }

    //setten van Index
    public void setIndex(int index) {
        this.index = index;
    }
}
