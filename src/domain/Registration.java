package domain;

import java.time.LocalDate;

public class Registration {
    private String studentEmail;
    private String courseName;
    private LocalDate registrationDate;
    private int certificateId;

    public Registration(String student, String course, LocalDate registrationDate) {
        this.studentEmail = student;
        this.courseName = course;
        this.registrationDate = registrationDate;
    }

    public Registration(String student, String course, LocalDate registrationDate, int certificateID) {
        this(student, course, registrationDate);
        this.certificateId = certificateID;
    }

    //studentemail opvragen
    public String getStudent() {
        return this.studentEmail;
    }

    //coursnaam opvragen
    public String getCourse() {
        return this.courseName;
    }

    //registratiedatum opvragen
    public LocalDate getRegistrationDate() {
        return this.registrationDate;
    }

    //certificaatID opvragen
    public int getCertificateId() {
        return this.certificateId;
    }

    //Studentemail setten
    public void setStudent(String student) {
        this.studentEmail = student;
    }

    //Coursenaam setten
    public void setCourse(String course) {
        this.courseName = course;
    }

    //Registratiedatum setten
    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;          
    }

    //CertificaatID setten
    public void setCertificateId(int certificateId) {
        this.certificateId = certificateId;
    }

    @Override
    public String toString() {
        return this.studentEmail + " - " + this.courseName + " - " + this.registrationDate.toString();
    }
    
}
