package domain;
/**
 * @desc student domain class
 */
public class Student extends Person {
    
    private String dateOfBirth;
    private String gender;
    private String city;
    private String country;
    private String street;
    private int houseNumber;
    private String houseNumberAddendom;
    private String zipcode;

    public Student(String name, String email, String dateOfBirth, String gender, String city, String country,
        String street, int houseNumber, String houseNumberAddendom, String zipcode) {
        super(name, email);
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.city = city;
        this.country = country;
        this.street = street;
        this.houseNumber = houseNumber;
        this.houseNumberAddendom = houseNumberAddendom;
        this.zipcode = zipcode;
    }

    //geboortedatum van een student opvragen
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    //geboortedatum van een student setten
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    //geslacht van een student opvragen
    public String getGender() {
        return gender;
    }

    //geslacht van een student setten
    public void setGender(String gender) {
        this.gender = gender;
    }

    //woonplaats van een student opvragen
    public String getCity() {
        return city;
    }

    //woonplaats van een student setten
    public void setCity(String city) {
        this.city = city;
    }

    //land van een student opvragen
    public String getCountry() {
        return country;
    }

    //land van een student setten
    public void setCountry(String country) {
        this.country = country;
    }

    //straatnaam van een student opvragen
    public String getStreet() {
        return street;
    }

    //straatnaam van een student setten
    public void setStreet(String street) {
        this.street = street;
    }

    //huisnummer van een student opvragen
    public int getHouseNumber() {
        return houseNumber;
    }

    //huisnummer van een student setten
    public void setHouseNumber(int houseNumber) {
        this.houseNumber = houseNumber;
    }

    //huisnummeraddon van een student opvragen
    public String getHouseNumberAddendom() {
        return houseNumberAddendom;
    }

    //huisnummeraddon van een student setten
    public void setHouseNumberAddendom(String houseNumberAddendom) {
        this.houseNumberAddendom = houseNumberAddendom;
    }

    //postcode van een student opvragen
    public String getZipcode() {
        return zipcode;
    }

    //postcode van een student setten
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

}
