package domain;

public class Progress {
    private double percentage;
    private Person student;
    private ContentItem contentItem;

    public Progress(Student student, ContentItem contentItem, double percentage) {
        this.student = student;
        this.contentItem = contentItem;
        this.percentage = percentage;
    }

    //Ophalen van percentage
    public double getPercentage() {
        return this.percentage;
    }

    //Setten van percentage
    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    //Ophalen van Student
    public Person getStudent() {
        return this.student;
    }

    //Setten van student
    public void setStudent(Person student) {
        this.student = student;
    }

    //Ophalen van ContentItem
    public ContentItem getContentItem() {
        return this.contentItem;
    }

    //Setten van ContentItem
    public void setContentItem(ContentItem contentItem) {
        this.contentItem = contentItem;
    }
}
