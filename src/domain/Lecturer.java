package domain;

public class Lecturer extends Person {
    private String organisation;

    public Lecturer(String name, String email, String organisation) {
        super(name, email);
        this.organisation = organisation;
    }

    //ophalen van Organisatie
    public String getOrganisation() {
        return organisation;
    }

    //setten van Organisatie
    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }


}
