package domain;

abstract class Person {
    private String name;
    private String email;
    
    public Person(String name, String email) {
        this.name = name;
        this.email = email;
    }

    //ophalen van Naam
    public String getName() {
        return name;
    }

    //setten van Naam
    public void setName(String name) {
        this.name = name;
    }

    //ophalen van Email
    public String getEmail() {
        return email;
    }

    //setten van Email
    public void setEmail(String email) {
        this.email = email;
    }
}
