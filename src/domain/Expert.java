package domain;

public class Expert extends Person {
    private String subject;

    public Expert(String name, String email, String subject) {
        super(name, email);
        this.subject = subject;
    }

    //ophalen van vak
    public String getSubject() {
        return subject;
    }

    //setten van vak
    public void setSubject(String subject) {
        this.subject = subject;
    }

}
