package domain;

public class ContactPerson extends Person {
    public ContactPerson(String name, String email) {
        super(name, email);
    }
}
