package domain;

public class Certificate {
    private int ID;
    private double grade;
    private String nameOfExpert;
    private Registration registration;

    public Certificate(double grade, String nameOfExpert) {
        this.grade = grade;
        this.nameOfExpert = nameOfExpert;
        this.registration = null;
    }

    public Certificate(int ID, double grade, String nameOfExpert) {
        this(grade, nameOfExpert);
        this.ID = ID;
    }

    public Certificate(double grade, String nameOfExpert, Registration registration) {
        this(grade, nameOfExpert);
        this.registration = registration;
    }

    //Ophalen van certificaatId
    public int getID() {
        return this.ID;
    }

    //setten van certificaatId
    public void setID(int ID) {
        this.ID = ID;
    }

    //Ophalen van cijfer
    public double getGrade() {
        return this.grade;
    }

    //setten van cijfer
    public void setGrade(double grade) {
        this.grade = grade;
    }

    //Ophalen van docent / Expert
    public String getNameOfExpert() {
        return this.nameOfExpert;
    }

    //setten van docent / Expert
    public void setNameOfExpert(String nameOfExpert) {
        this.nameOfExpert = nameOfExpert;
    }

    //Ophalen van registratie
    public Registration getRegistration() {
        return this.registration;
    }

    //setten van registratie
    public void setRegistration(Registration registration) {
        this.registration = registration;
    }
}
