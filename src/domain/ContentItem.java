package domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public abstract class ContentItem {
    private int id;
    private LocalDate publicationDate;
    private String title;
    private String description;
    private List<Progress> progressions;

    public ContentItem(int id, LocalDate publicationDate, String title, String description) {
        this.id = id;
        this.publicationDate = publicationDate;
        this.title = title;
        this.description = description;
        this.progressions = new ArrayList<>();
    }

    //ophalen van contentId
    public int getId() {
        return this.id;
    }

    //setten van contentId
    public void setId(int id) {
        this.id = id;
    }

    //ophalen van publicatiedatum
    public LocalDate getPublicationDate() {
        return this.publicationDate;
    }

    //setten van publicatiedatum
    public void setPublicationDate(LocalDate publicationDate) {
        this.publicationDate = publicationDate;
    }

    //ophalen van titel
    public String getTitle() {
        return this.title;
    }

    //setten van titel
    public void setTitle(String title) {
        this.title = title;
    }

    //ophalen van beschrijving
    public String getDescription() {
        return this.description;
    }

    //setten van beschrijving
    public void setDescription(String description) {
        this.description = description;
    }

    //ophalen van huidige progressies
    public List<Progress> getProgressions() {
        return this.progressions;
    }

    //setten van huidige progressies
    public void setProgressions(List<Progress> progressions) {
        this.progressions = progressions;
    }

    //toevoegen huidige progressie
    public void addProgress(Progress progress) {
        this.progressions.add(progress);
    }

    //opvragen gemiddelde progressie
    public double getAverageProgression() {
        if (this.progressions.size() == 0) {
            return 0;
        }
        
        return this.progressions.stream().mapToDouble((p) -> p.getPercentage()).average().getAsDouble();
    }
}
