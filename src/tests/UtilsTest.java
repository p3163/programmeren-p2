package tests;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

import logic.Utils;

public class UtilsTest {
/* @subcontract 31 days in month {
     *   @requires (month == 1 || month == 3 || month == 5 || month == 7 ||
     *             month == 8 || month == 10 || month == 12) && 1 <= day <= 31;
     *   @ensures \result = true; }
     */

    @Test
    public void testValidateDateRequiresMonth1WithDay1EnsuresTrue() {
       // arrange
       int day = 1;
       int month = 1;
       int year = LocalDate.now().getYear() + 1;

       // act
       boolean result = Utils.validateDate(day, month, year);

       // assert
       assertEquals(true, result);
    }

     @Test
     public void testValidateDateRequiresMonth5WithDay15EnsuresTrue() {
        // arrange
        int day = 15;
        int month = 5;
        int year = LocalDate.now().getYear() + 1;

        // act
        boolean result = Utils.validateDate(day, month, year);

        // assert
        assertEquals(true, result);
     }

     @Test
     public void testValidateDateRequiresMonth8WithDay31EnsuresTrue() {
        // arrange
        int day = 31;
        int month = 8;
        int year = LocalDate.now().getYear() + 1;

        // act
        boolean result = Utils.validateDate(day, month, year);

        // assert
        assertEquals(true, result);
     }

     /* @subcontract 30 days in month {
      * @requires (month == 4 || month == 6 || month == 9 || month == 11) &&
      *           1 <= day <= 30;
      * @ensures \result = true; }
      */

    @Test
    public void testValidateDateRequiresMonth6WithDay1EnsuresTrue() {
        // arrange
        int day = 1;
        int month = 6;
        int year = LocalDate.now().getYear() + 1;

        // act
        boolean result = Utils.validateDate(day, month, year);

        // assert
        assertEquals(true, result);
    }

    @Test
    public void testValidateDateRequiresMonth4WithDay25EnsuresTrue() {
        // arrange
        int day = 25;
        int month = 4;
        int year = LocalDate.now().getYear() + 1;

        // act
        boolean result = Utils.validateDate(day, month, year);

        // assert
        assertEquals(true, result);
    }

    @Test
    public void testValidateDateRequiresMonth9WithDay30EnsuresTrue() {
        // arrange
        int day = 30;
        int month = 9;
        int year = LocalDate.now().getYear() + 1;

        // act
        boolean result = Utils.validateDate(day, month, year);

        // assert
        assertEquals(true, result);
    }

    /* @subcontract 29 days in month {
     * @requires month == 2 && 1 <= day <= 29 &&
     *           (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
     * @ensures \result = true; }
     */

    @Test
    public void testValidateDateRequiresYear400WithMonth2WithDay1EnsuresTrue() {
        // arrange
        int day = 29;
        int month = 2;
        int year = 400;

        // act
        boolean result = Utils.validateDate(day, month, year);

        // assert
        assertEquals(true, result);
    }

    @Test
    public void testValidateDateRequiresYear800WithMonth2WithDay29EnsuresTrue() {
        // arrange
        int day = 29;
        int month = 2;
        int year = 800;

        // act
        boolean result = Utils.validateDate(day, month, year);

        // assert
        assertEquals(true, result);
    }

    @Test
    public void testValidateDateRequiresYear1616WithMonth2WithDay15EnsuresTrue() {
        // arrange
        int day = 15;
        int month = 2;
        int year = 1616;

        // act
        boolean result = Utils.validateDate(day, month, year);

        // assert
        assertEquals(true, result);
    }

    /* @subcontract 28 days in month {
     * @requires month == 2 && 1 <= day <= 28 &&
     *           (year % 4 != 0 || (year % 100 == 0 && year % 400 != 0));
     * @ensures \result = true; }
     */

    @Test
    public void testValidateDateRequiresYear2001WithMonth2WithDay1EnsuresTrue() {
        // arrange
        int day = 1;
        int month = 2;
        int year = 2001;

        // act
        boolean result = Utils.validateDate(day, month, year);

        // assert
        assertEquals(true, result);
    }

    @Test
    public void testValidateDateRequiresYear2001WithMonth2WithDay28EnsuresTrue() {
        // arrange
        int day = 28;
        int month = 2;
        int year = 2001;

        // act
        boolean result = Utils.validateDate(day, month, year);

        // assert
        assertEquals(true, result);
    }

    @Test
    public void testValidateDateRequiresYear3000WithMonth2WithDay14EnsuresTrue() {
        // arrange
        int day = 14;
        int month = 2;
        int year = 3000;

        // act
        boolean result = Utils.validateDate(day, month, year);

        // assert
        assertEquals(true, result);
    }

    /* @subcontract all other cases {
     * @requires no other accepting precondition;
     * @ensures \result = false; }
     */

    @Test
    public void testValidateDateRequiresMonthNegative1WithDay5EnsuresFalse() {
        // arrange
        int day = 0;
        int month = -1;
        int year = LocalDate.now().getYear() + 1;

        // act
        boolean result = Utils.validateDate(day, month, year);

        // assert
        assertEquals(false, result);
    }

    @Test
    public void testValidateDateRequiresMonth13WithDay5EnsuresFalse() {
        // arrange
        int day = 0;
        int month = -1;
        int year = LocalDate.now().getYear() + 1;

        // act
        boolean result = Utils.validateDate(day, month, year);

        // assert
        assertEquals(false, result);
    }

    @Test
    public void testValidateDateRequiresMonth1WithDay0EnsuresFalse() {
        // arrange
        int day = 0;
        int month = 1;
        int year = LocalDate.now().getYear() + 1;

        // act
        boolean result = Utils.validateDate(day, month, year);

        // assert
        assertEquals(false, result);
    }

    @Test
    public void testValidateDateRequiresMonth1WithDay37EnsuresFalse() {
        // arrange
        int day = 37;
        int month = 1;
        int year = LocalDate.now().getYear() + 1;

        // act
        boolean result = Utils.validateDate(day, month, year);

        // assert
        assertEquals(false, result);
    }

    @Test
    public void testValidateDateRequiresMonth4WithDay0EnsuresFalse() {
        // arrange
        int day = 0;
        int month = 4;
        int year = LocalDate.now().getYear() + 1;

        // act
        boolean result = Utils.validateDate(day, month, year);

        // assert
        assertEquals(false, result);
    }

    @Test
    public void testValidateDateRequiresMonth6WithDay42EnsuresFalse() {
        // arrange
        int day = 42;
        int month = 6;
        int year = LocalDate.now().getYear() + 1;

        // act
        boolean result = Utils.validateDate(day, month, year);

        // assert
        assertEquals(false, result);
    }

    @Test
    public void testValidateDateRequiresMonth2WithDay0EnsuresFalse() {
        // arrange
        int day = 0;
        int month = 2;
        int year = LocalDate.now().getYear() + 1;

        // act
        boolean result = Utils.validateDate(day, month, year);

        // assert
        assertEquals(false, result);
    }

    @Test
    public void testValidateDateRequiresMonth2WithDay35EnsuresFalse() {
        // arrange
        int day = 35;
        int month = 2;
        int year = LocalDate.now().getYear() + 1;

        // act
        boolean result = Utils.validateDate(day, month, year);

        // assert
        assertEquals(false, result);
    }

    @Test
    public void testValidateDateRequiresYear500Month2WithDay29EnsuresFalse() {
        // arrange
        int day = 29;
        int month = 2;
        int year = 500;

        // act
        boolean result = Utils.validateDate(day, month, year);

        // assert
        assertEquals(false, result);
    }

    @Test(expected = NullPointerException.class)
    public void TestFormatPostalCodeSignalsNullPointerException() {
        // Arrange
        String input = null;
        // Act
        Utils.formatPostalCode(input);
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestFormatPostalCodeSignalsIllegalArgumentException() {
        // Arrange
        String input = "12345 AB";
        // Act
        Utils.formatPostalCode(input);
    }

    @Test
    public void TestFormatPostalCodeInsuresCapitalLetters() {
        // Arrange
        String input = "1235 ab";
        // Act
        String result = Utils.formatPostalCode(input);
        // Assert
        assertEquals("1235 AB", result);
    }

    @Test
    public void TestFormatPostalCodeEnsuresTrue() {
        // Arrange
        String input = "1235 AB";
        // Act
        String result = Utils.formatPostalCode(input);
        // Assert
        assertEquals("1235 AB", result);
    }

    @Test
    public void TestValideEmailWithTwoAtsRequiresOneAtEnsuresFalse() {
        //Arrange
        String input = "test@gmail.com@";
        //Act
        Boolean result = Utils.validateemail(input);
        //Assert
        assertEquals(false, result);
    }
    @Test
    public void TestValidateEmailBoxPartRequiresNullEnsuresFalse() {
        //Arrange
        String input = "@gmail.com";
        //Act
        Boolean result = Utils.validateemail(input);
        //Assert
        assertEquals(false, result);
    }
    @Test
    public void TestSubDomainDelimiterRequiresOneDotEnsuresFalse() {
        //Arrange
        String input = "test@hotmail.test.nl";
        //Act
        Boolean result = Utils.validateemail(input);
        //Assert
        assertEquals(false, result);
    }
    @Test
    public void TestNoSubdomainPartRequiresNoSubDomainEnsuresFalse() {
        //Arrange
        String input = "thimo@com";
        //Act
        Boolean result = Utils.validateemail(input);
        //Assert
        assertEquals(false, result);
    }
    @Test
    public void TestNoTldPartRequiresNoTldEnsuresFalse() {
        //Arrange
        String input = "thimo@gmail.";
        //Act
        Boolean result = Utils.validateemail(input);
        //Assert
        assertEquals(false, result);
    }
    @Test
    public void TestValidEmailEnsuresTrue() {
        //Arrange
        String input = "thimo@gmail.com";
        //Act
        Boolean result = Utils.validateemail(input);
        //Assert
        assertEquals(true, result);
    }

    @Test
    public void testNumericRangeToolsRequiresGrade0EnsuresTrue() {
        // arrange
        int grade = 0;

        // act
        boolean result = Utils.isValidGrade(grade);

        // assert
        assertEquals(true, result);
    }

    @Test
    public void testNumericRangeToolsRequiresGrade50EnsuresTrue() {
        // arrange
        int grade = 50;

        // act
        boolean result = Utils.isValidGrade(grade);

        // assert
        assertEquals(true, result);
    }

    @Test
    public void testNumericRangeToolsRequiresGrade100EnsuresTrue() {
        // arrange
        int grade = 100;

        // act
        boolean result = Utils.isValidGrade(grade);

        // assert
        assertEquals(true, result);
    }

    /* @subcontract value out of range low {
     *   @requires Grade < 0;
     *   @ensures \result = false; }
     */

    @Test
    public void testNumericRangeToolsRequiresGradeNegative5EnsuresFalse() {
        // arrange
        int grade = -5;

        // act
        boolean result = Utils.isValidGrade(grade);

        // assert
        assertEquals(false, result);
    }

    /* @subcontract value out of range high {
     *   @requires Grade > 100;
     *   @signals \result = false; }
     */

    @Test
    public void testNumericRangeToolsRequiresGradeNegative110EnsuresFalse() {
        // arrange
        int grade = 110;

        // act
        boolean result = Utils.isValidGrade(grade);

        // assert
        assertEquals(false, result);
    }

}
