package logic;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import dal.RegistrationDao;
import domain.Registration;

public class RegistrationService {
    private RegistrationDao registrationDao = new RegistrationDao();

    //Alle registraties uit de database ophalen
    public List<Registration> getAllRegistrations() throws SQLException {
        return registrationDao.selectAll();
    }

    //Aanmaken van een registratie
    public void createRegistration(String studentEmail, String courseName, int day, int month, int year) throws Exception {
        if (!Utils.validateDate(day, month, year)) {
            throw new IllegalArgumentException("Invalid date");
        }

        if (!Utils.validateemail(studentEmail)) {
            throw new IllegalArgumentException("Email was not valid");
        }

        registrationDao.insert(new Registration(studentEmail, courseName, LocalDate.of(year, month, day)));
    }

    //Geselecteerde registratie uit de database ophalen
    public Registration getRegistration(Registration registration) throws SQLException {
        return registrationDao.select(registration);
    }

    //Geselecteerde registratie uit de database verwijderen
    public void deleteRegistration(Registration registration) throws SQLException {
        registrationDao.delete(registration);
    }

    //Alle beschikbare registraties uit de database ophalen
    public List<Registration> getAllAvailableRegistrations() throws SQLException {
        return registrationDao.selectAvailable();
    }

    //Geselecteerde registratie uit de database updaten
    public void updateRegistration(Registration registration) throws SQLException {
        if (!Utils.validateemail(registration.getStudent())) {
            throw new IllegalArgumentException("Email was not valid");
        }

        registrationDao.update(registration);
    }

    //Alle beschikbare registraties gerelateerde aan een course uit de database ophalen
    public List<Registration> getAllRegistrationsRelatedToCourse(String courseName) throws SQLException {
        return registrationDao.selectForCourse(courseName);
    }

}
