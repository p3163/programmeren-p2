package logic;

import java.sql.SQLException;
import java.util.List;

import dal.CourseDao;
import domain.Course;

/**
 * @desc Handles course related events from UI
 */
public class CourseService {
    private CourseDao courseDao = new CourseDao();
    private RegistrationService registrationService = new RegistrationService();

    //Alle courses uit de database halen
    public List<Course> getAllCourses() throws SQLException {
        return courseDao.selectAll();
    }

    //Alle populaire courses uit de database halen
    public List<Course> getPopularCourses() throws SQLException {
        return courseDao.selectPopular();
    }

    //Alle gerelateerde courses uit de database halen
    public List<Course> getRelatedCourses(Course course) throws SQLException {
        return courseDao.selectRelated(course);
    }

    //Aanmaken van een course
    public void createCourse(String name, String introText, String subject, String difficulty) throws SQLException {
        courseDao.insert(new Course(name, introText, subject, difficulty));
    }

    //Geselecteerde course uit de database halen
    public Course getCourse(Course search) throws SQLException {
        Course course = courseDao.select(search);
        course.setRegistrations(registrationService.getAllRegistrationsRelatedToCourse(search.getName()));
        return course;
    }

    //Geselecteerde course uit de database verwijderen
    public void deleteCourse(Course course) throws SQLException {
        courseDao.delete(course);
    }

    //Geselecteerde course uit de database updaten
    public void updateCourse(Course course) throws SQLException {
        courseDao.update(course);
    }
} 
