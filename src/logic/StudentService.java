package logic;
import java.sql.SQLException;
import java.util.List;

import dal.Dao;
import dal.StudentDao;
import domain.Student;

public class StudentService {
    private Dao<Student> studentDao = new StudentDao();

    public List<Student> getAllStudents() throws SQLException {
        return studentDao.selectAll();
    }

    //Aanmaken van een student
    public void createStudent(String name, String email, String dateOfBirth, String gender, String city, String country, String street, int houseNumber, String houseNumberAddendom, String zipcode) throws SQLException {
        if(!Utils.validateemail(email)) {
            throw new IllegalArgumentException("Email was not valid");
        }
        String formattedZipCode = Utils.formatPostalCode(zipcode);
        studentDao.insert(new Student(name, email, dateOfBirth, gender, city, country, street, houseNumber, houseNumberAddendom, formattedZipCode));
    }

    //Het updaten van een student uit de database
    public void updateStudent(String name, String email, String dateOfBirth, String gender, String city, String country, String street, int houseNumber, String houseNumberAddendom, String zipcode) throws SQLException {
        if(!Utils.validateemail(email)) {
            throw new IllegalArgumentException();
        }
        String formattedZipCode = Utils.formatPostalCode(zipcode);
        studentDao.update(new Student(name, email, dateOfBirth, gender, city, country, street, houseNumber, houseNumberAddendom, formattedZipCode));
    }

    //Het opvragen van een geselecteerde student uit de database
    public Student getStudent(Student student) throws SQLException {
        return studentDao.select(student);
    }

    //Het verwijderen van een geselecteerde student uit de database
    public void deleteStudent(Student student) throws SQLException{
        studentDao.delete(student);
    }

}
