package logic;

import java.sql.SQLException;
import java.util.List;

import dal.ModuleDao;
import domain.Module;

public class ModuleService {
    private ModuleDao moduleDao = new ModuleDao();

    //Alle modules uit de database ophalen
    public List<Module> getAllAvailableModules() throws SQLException {
        return moduleDao.selectAvailable();
    }

    //Alle modules uit de database updaten
    public void updateAllModules(String courseName, List<String> titles) throws SQLException {
        moduleDao.updateAll(courseName, titles);
    }

    //Geselecteerde module uit de database ophalen
    public List<Module> getModules(String course) throws SQLException {
        return moduleDao.select(course);
    }
}
