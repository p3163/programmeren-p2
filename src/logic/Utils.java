package logic;

import java.util.Arrays;

public class Utils {
    // valideren en formatteren postalcode
    public static String formatPostalCode(String postalCode) {
        if(postalCode == null) {
            throw new NullPointerException();
        }

        if(Integer.valueOf(postalCode.trim().substring(0, 4)) > 999 &&
                     Integer.valueOf(postalCode.trim().substring(0, 4)) <= 9999 &&
                     postalCode.trim().substring(4).trim().length() == 2 &&
                     'A' <= postalCode.trim().substring(4).trim().toUpperCase().charAt(0) &&
                     'Z' >= postalCode.trim().substring(4).trim().toUpperCase().charAt(0)) {
                        return  postalCode.trim().substring(0, 4) + " " + postalCode.trim().substring(4).trim().toUpperCase();
        } else {
            throw new IllegalArgumentException();
        }
    }
    // valideren email
    public static Boolean validateemail(String email) {
        //Two ats found
        if (!email.contains("@") || email.indexOf("@") != email.lastIndexOf("@")) {
            return false;
        }
        //no mailbox part
        if (!email.contains("@") || email.split("@")[0].length() < 1) {
            return false;
        }
        //subdomain-tld delimiter
        if (!email.contains("@") || email.split("@")[1].split("\\.").length > 2) {
            return false;
        }
        //no subdomain part
        if (!email.contains("@") || !email.contains(".") || email.split("@")[1].split("\\.")[0].length() < 1) {
            return false;
        }
        //no tld part
        if (!email.contains("@") || Arrays.stream(email.split("@")[1].split("\\.")).count() <= 1 || email.split("@")[1].split("\\.")[1].length() < 1) {
            return false;
        }
        //valid email
        return true;
    }

    //Valideren van de coursedatum
    public static boolean validateDate(int day, int month, int year) {
        if ((month == 1 || month == 3 || month == 5 || month == 7 ||
                month == 8 || month == 10 || month == 12) && day >= 1 && day <= 31) {
            return true;
        }

        if ((month == 4 || month == 6 || month == 9 || month == 11) &&
                day >= 1 && day <= 30) {
            return true;
        }

        if (month == 2 && day >= 1 && day <= 29 &&
                (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0))) {
            return true;
        }

        if (month == 2 && day >= 1 && day <= 28 &&
                (year % 4 != 0 || (year % 100 == 0 && year % 400 != 0))) {
            return true;
        }

        return false;
    }
     
    //Geselecteerd certificaatcijfer valideren
    public static boolean isValidGrade(double grade) {
        return grade >= 0 && grade <= 10;
    }
}
