package logic;

import java.sql.SQLException;
import java.util.List;

import dal.CertificateDao;
import domain.Certificate;
import domain.Registration;

public class CertificateService {
    private CertificateDao certificateDao = new CertificateDao();

    //Aanmaken van een certificaat
    public void createCertificate(double grade, String nameOfExpert, Registration registration) throws Exception {
        if (!Utils.isValidGrade(grade)) {
            throw new IllegalArgumentException();
        }

        certificateDao.insert(new Certificate(grade, nameOfExpert, registration));
    }

    //Alle certificaten uit de database halen
    public List<Certificate> getAllCertificates() throws SQLException {
        return certificateDao.selectAll();
    }

    //Geselecteerde certificaat uit de database updaten
    public void updateCertificate(Certificate certificate, double grade, String nameOfExpert) throws Exception {
        if (!Utils.isValidGrade(grade)) {
            throw new IllegalArgumentException();
        }

        certificate.setGrade(grade);
        certificate.setNameOfExpert(nameOfExpert);
        
        certificateDao.update(certificate);
    }

    //Geselecteerde certificaat uit de database verwijderen
    public void deleteCertificate(Certificate certificate) throws SQLException {
        certificateDao.delete(certificate);
    }

} 
