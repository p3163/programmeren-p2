package dal;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import domain.Certificate;

public class CertificateDao implements Dao<Certificate> {

    /**
     * @desc selects a certificate from the database
     */
    @Override
    public Certificate select(Certificate certificate) throws SQLException {
        return null;
    }

    /**
     * @desc selects all certificates from the database
     */
    @Override
    public List<Certificate> selectAll() throws SQLException {
        List<Certificate> certificates = new ArrayList<>();

        Connection con = DBConnection.getConnection();

        Statement stmt = con.createStatement(); 
        ResultSet rs = stmt.executeQuery("SELECT * FROM Certificate;");

        while (rs.next()) {
            certificates.add(new Certificate(rs.getInt("CertificateID"), rs.getDouble("Grade"), rs.getString("NameOfExpert")));
        }

        return certificates;
    }

    /**
     * @desc Inserts certificate into the database
     */
    @Override
    public void insert(Certificate certificate) throws SQLException {
        Connection con = DBConnection.getConnection();

        Statement stmt = con.createStatement(); 
        stmt.execute("INSERT INTO Certificate (Grade, NameOfExpert) VALUES (" + certificate.getGrade() + ", '" + certificate.getNameOfExpert() + "');");
    }

    /**
     * @desc updates certificate from the database
     */
    @Override
    public void update(Certificate certificate) throws SQLException {
        Connection con = DBConnection.getConnection();
        Statement stmt = con.createStatement();

        stmt.execute(String.format("UPDATE Certificate SET Grade = " + certificate.getGrade() + ", NameOfExpert='%s' WHERE CertificateID = %o;", certificate.getNameOfExpert(), certificate.getID()));
    }

    /**
     * @desc Deletes certificate from the database
     */
    @Override
    public void delete(Certificate certificate) throws SQLException {
        Connection con = DBConnection.getConnection();
        Statement stmt = con.createStatement();

        stmt.execute(String.format("DELETE FROM Certificate WHERE CertificateID = %o;", certificate.getID()));
    }
}
