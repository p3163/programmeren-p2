package dal;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import domain.Course;

public class CourseDao implements Dao<Course> {

    /**
     * @desc selects a course from the database
     */
    @Override
    public Course select(Course course) throws SQLException {
        Connection con = DBConnection.getConnection();

        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(String.format("SELECT * FROM Course WHERE CourseName='%s';", course.getName()));

        if (rs.next()) {
            return new Course(rs.getString("CourseName"), rs.getString("IntroductionText"), rs.getString("Subject"), rs.getString("Difficulty"));
        }

        throw new SQLException();
    }

    /**
     * @desc returns all the course records from the database
     */
    @Override
    public List<Course> selectAll() throws SQLException {
        List<Course> courses = new ArrayList<>();

        Connection con = DBConnection.getConnection();

        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM Course;");

        while (rs.next()) {
            courses.add(new Course(rs.getString("CourseName"), rs.getString("IntroductionText"), rs.getString("Subject"), rs.getString("Difficulty")));
        }

        return courses;
    }

    /**
     * @desc returns the popular courses, filtered by the amount of registrations
     */
    public List<Course> selectPopular() throws SQLException {
        List<Course> courses = new ArrayList<>();

        Connection con = DBConnection.getConnection();

        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT Course.CourseName, Course.IntroductionText, Course.Subject, Course.Difficulty FROM Course LEFT OUTER JOIN Registration ON Registration.CourseName = Course.CourseName LEFT JOIN Certificate ON Certificate.CertificateID = Registration.CertificateID GROUP BY Course.CourseName, Course.IntroductionText, Course.Subject, Course.Difficulty ORDER BY Count(Course.CourseName) DESC, Course.CourseName ASC;");

        while (rs.next()) {
            courses.add(new Course(rs.getString("CourseName"), rs.getString("IntroductionText"), rs.getString("Subject"), rs.getString("Difficulty")));
        }

        return courses;
    }
    
    /**
     * @desc Selects related courses from database
     */
    public List<Course> selectRelated(Course course) throws SQLException {
        List<Course> courses = new ArrayList<>();

        Connection con = DBConnection.getConnection();

        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT top 3 * FROM Course WHERE Course.CourseName LIKE '%" + course.getName().substring(0, course.getName().indexOf(" ")) + "%' AND Course.CourseName != '" + course.getName() + "';");

        while (rs.next()) {
            courses.add(new Course(rs.getString("CourseName"), rs.getString("IntroductionText"), rs.getString("Subject"), rs.getString("Difficulty")));
        }

        return courses;
    }
    /**
     * @desc inserts course into database
     */
    @Override
    public void insert(Course course) throws SQLException {
        Connection con = DBConnection.getConnection();

        Statement stmt = con.createStatement();
        stmt.execute(String.format("INSERT INTO Course VALUES ('%s', '%s', '%s', '%s');", course.getName(), course.getIntroduction(), course.getSubject(), course.getDifficulty()));
    }

    /**
     * @desc updates a course from the database
     */
    @Override
    public void update(Course course) throws SQLException {
        Connection con = DBConnection.getConnection();
        Statement stmt = con.createStatement();

        stmt.execute(String.format("UPDATE Course SET IntroductionText='%s', Subject='%s', Difficulty='%s' WHERE CourseName = '%s';", course.getIntroduction(), course.getSubject(), course.getDifficulty(), course.getName()));
    }

    /**
     * @desc deletes a course from the database
     */
    @Override
    public void delete(Course course) throws SQLException {
        Connection con = DBConnection.getConnection();

        Statement stmt = con.createStatement();
        stmt.execute(String.format("DELETE FROM Course WHERE CourseName = '%s';", course.getName()));
    }
}
