package dal;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import domain.Student;

public class StudentDao implements Dao<Student> {

    /**
     * @desc selects student from database
     */
    @Override
    public Student select(Student student) throws SQLException {
        Connection con = DBConnection.getConnection();

        Statement stmt = con.createStatement(); 
        ResultSet rs = stmt.executeQuery(String.format("SELECT * FROM Student WHERE Email='%s';", student.getEmail()));

        if (rs.next()) {
            return new Student(rs.getString("Name"), rs.getString("Email"), rs.getString("DateOfBirth"), rs.getString("Gender"), rs.getString("City"), rs.getString("Country"), rs.getString("Street"), rs.getInt("HouseNumber"), rs.getString("HouseNumberAddendom"), rs.getString("Zipcode"));
        } else {
            throw new SQLException();
        }
    }

    /**
     * @desc returns all the student records from the database
     */
    @Override
    public List<Student> selectAll() throws SQLException {
        List<Student> students = new ArrayList<>();

        Connection con = DBConnection.getConnection();

        Statement stmt = con.createStatement(); 
        ResultSet rs = stmt.executeQuery("SELECT * FROM Student;");

        while (rs.next()) {
           students.add(new Student(rs.getString("Name"), rs.getString("Email"), rs.getString("DateOfBirth"), rs.getString("Gender"), rs.getString("City"), rs.getString("Country"), rs.getString("Street"), rs.getInt("HouseNumber"), rs.getString("HouseNumberAddendom"), rs.getString("Zipcode")));
        }

        return students;
    }

    /**
     * @desc inserts student into database
     */
    @Override
    public void insert(Student student) throws SQLException {
        Connection con = DBConnection.getConnection();

        Statement stmt = con.createStatement(); 
        stmt.execute(String.format("INSERT INTO Student VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');", student.getEmail(), student.getName(), student.getDateOfBirth(), student.getGender(), student.getCity(), student.getCountry(), student.getStreet(), student.getHouseNumber(), student.getHouseNumberAddendom(), student.getZipcode()));
    }

    /**
     * @desc updates student in database
     */
    @Override
    public void update(Student student) throws SQLException {
        Connection con = DBConnection.getConnection();

        Statement stmt = con.createStatement(); 
        stmt.execute(String.format("UPDATE Student Name = '%s', DateOfBirth = '%s', Gender = '%s', City = '%s', Country = '%s', Street = '%s', HouseNumber = '%s', HouseNumberAddendom = '%s', Zipcode = '%s' WHERE email = '%s';", student.getName(), student.getDateOfBirth(), student.getGender(), student.getCity(), student.getCountry(), student.getStreet(), student.getHouseNumber(), student.getHouseNumberAddendom(), student.getZipcode(), student.getEmail()));
    }

    /**
     * @desc deletes student in database
     */
    @Override
    public void delete(Student value) throws SQLException {
        Connection con = DBConnection.getConnection();

        Statement stmt = con.createStatement(); 
        stmt.execute(String.format("DELETE FROM Student WHERE Email = '%s';", value.getEmail()));
    }
}
