package dal;

import java.sql.SQLException;
import java.util.List;

/**
 * @desc interface for data access object
 */
public interface Dao<T> {
    T select(T value) throws SQLException;
    List<T> selectAll() throws SQLException;
    void insert(T value) throws SQLException;
    void update(T value) throws SQLException;
    void delete(T value) throws SQLException;
}
