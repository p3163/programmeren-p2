package dal;

import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import domain.Module;
import domain.Progress;

public class ModuleDao {

    /**
     * @desc selects available modules in database
     */
    public List<Module> selectAvailable() throws SQLException {
        Connection con = DBConnection.getConnection();
        List<Module> modules = new ArrayList<>();

        Statement stmt = con.createStatement(); 
        ResultSet rs = stmt.executeQuery("SELECT * FROM Module INNER JOIN ContentItem ON Module.ContentID = ContentItem.ContentID WHERE CourseName IS NULL;");

        while (rs.next()) {
            modules.add(new Module(rs.getInt("ContentID"), rs.getDate("PublicationDate").toLocalDate(), rs.getString("Title"), rs.getString("Description"), rs.getDouble("Version"), rs.getInt("IndexNumber")));
        }

        return modules;
    }

    /**
     * @desc updates all modules in database
     */
    public void updateAll(String courseName, List<String> titles) throws SQLException {
        Connection con = DBConnection.getConnection();
        Statement stmt = con.createStatement();
        
        for (String title : titles) {
            stmt.execute(String.format("UPDATE Module SET CourseName='%s' WHERE Title='%s';", courseName, title));
        }
    }

    /**
     * @desc selects a module from the database
     */
    public List<Module> select(String course) throws SQLException {
        Connection con = DBConnection.getConnection();
        List<Module> modules = new ArrayList<>();

        Statement stmt = con.createStatement(); 
        ResultSet rs = stmt.executeQuery(String.format("SELECT Module.ContentID, Title, Version, IndexNumber, PublicationDate, Description, AVG(Percentage) AS AVGPercentage " +
                                                            "FROM MODULE " +
                                                            "INNER JOIN ContentItem ON Module.ContentID = ContentItem.ContentID " +
                                                            "INNER JOIN Progress ON ContentItem.ContentID = Progress.ContentID " +
                                                            "WHERE CourseName = '%s' " +
                                                            "GROUP BY Module.ContentID, Title, Version, IndexNumber, Description, PublicationDate;", course));

        while (rs.next()) {
            Module module = new Module(rs.getInt("ContentID"), 
                                            rs.getDate("PublicationDate").toLocalDate(), 
                                            rs.getString("Title"),
                                            rs.getString("Description"), 
                                            rs.getDouble("Version"), 
                                            rs.getInt("IndexNumber"));

            Progress progress = new Progress(null, module, rs.getDouble("AVGPercentage"));
            module.addProgress(progress);
            modules.add(module);
        }

        return modules;
    } 
}
