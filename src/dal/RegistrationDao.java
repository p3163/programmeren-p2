package dal;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import domain.Registration;

public class RegistrationDao implements Dao<Registration> {

    /**
     * @desc selects one registration from database
     */
    @Override
    public Registration select(Registration registration) throws SQLException {
        Connection con = DBConnection.getConnection();
        Statement stmt = con.createStatement(); 
        ResultSet rs = stmt.executeQuery(String.format("SELECT * FROM Registration WHERE RegisterDate='%s' AND CourseName='%s' AND StudentEmail='%s';", 
                                                registration.getRegistrationDate(), 
                                                registration.getCourse(),
                                                registration.getStudent()
                                                ));

        if (rs.next()) {
            return new Registration(rs.getString("StudentEmail"), rs.getString("CourseName"), rs.getDate("RegisterDate").toLocalDate(), rs.getInt("CertificateID"));
        } 
            
        throw new SQLException();
    }

    /**
     * @desc selects all Registrations from database
     */
    @Override
    public List<Registration> selectAll() throws SQLException {
        List<Registration> registrations = new ArrayList<>();

        Connection con = DBConnection.getConnection();
        Statement stmt = con.createStatement(); 
        ResultSet rs = stmt.executeQuery("SELECT * FROM Registration;");

        while (rs.next()) {
            registrations.add(new Registration(rs.getString("StudentEmail"), rs.getString("CourseName"), rs.getDate("RegisterDate").toLocalDate(), rs.getInt("CertificateID")));
        }

        return registrations;
    }

    /**
     * @desc inserts Registration into database
     */
    @Override
    public void insert(Registration registration) throws SQLException {
        Connection con = DBConnection.getConnection();
        Statement stmt = con.createStatement();
        stmt.execute(String.format("INSERT INTO Registration(RegisterDate, StudentEmail, CourseName) VALUES ('%s', '%s', '%s');", registration.getRegistrationDate().toString(), registration.getStudent(), registration.getCourse()));
    }

    /**
     * @desc updates Registration in database
     */
    @Override
    public void update(Registration registration) throws SQLException {
        Connection con = DBConnection.getConnection();
        Statement stmt = con.createStatement();
        // stmt.execute(String.format("UPDATE Registration SET RegisterDate='%s', StudentEmail='%s', CourseName='%s' WHERE ", registration.getRegistrationDate().toString(), registration.getStudent(), registration.getCourse()));
    } 

    /**
     * @desc deletes Registration from database
     */
    @Override
    public void delete(Registration registration) throws SQLException {
        Connection con = DBConnection.getConnection();
        Statement stmt = con.createStatement();

        stmt.execute(String.format("DELETE FROM Registration WHERE RegisterDate='%s' AND CourseName='%s' AND StudentEmail='%s';", 
                                registration.getRegistrationDate(), 
                                registration.getCourse(),
                                registration.getStudent()
                                ));
    }

    /**
     * @desc selects available Registrations from database
     */
    public List<Registration> selectAvailable() throws SQLException {
        Connection con = DBConnection.getConnection();
        List<Registration> registrations = new ArrayList<>();

        Statement stmt = con.createStatement(); 
        ResultSet rs = stmt.executeQuery("SELECT * FROM Registration WHERE CertificateID IS NULL;");

        while (rs.next()) {
            registrations.add(new Registration(rs.getString("StudentEmail"), rs.getString("CourseName"), rs.getDate("RegisterDate").toLocalDate()));
        }

        return registrations;
    }

    /**
     * @desc selects Registration from course in database
     */
    public List<Registration> selectForCourse(String courseName) throws SQLException {
        Connection con = DBConnection.getConnection();
        List<Registration> registrations = new ArrayList<>();

        Statement stmt = con.createStatement(); 
        ResultSet rs = stmt.executeQuery(String.format("SELECT * FROM Registration WHERE CourseName = '%s';", courseName));

        while (rs.next()) {
            registrations.add(new Registration(rs.getString("StudentEmail"), rs.getString("CourseName"), rs.getDate("RegisterDate").toLocalDate(), rs.getInt("CertificateID")));
        }

        return registrations;
    }
}   
