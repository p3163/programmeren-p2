USE master;
DROP DATABASE IF EXISTS Codecademy;
CREATE DATABASE Codecademy;
GO
USE Codecademy;

DROP TABLE IF EXISTS Course;
CREATE TABLE Course (
  CourseName nvarchar(50) PRIMARY KEY,
  IntroductionText nvarchar(500),
  Subject nvarchar(50),
  Difficulty nvarchar(20)
);

INSERT INTO Course (CourseName, IntroductionText, Subject, Difficulty) VALUES
('Javascript basics', 'Lorem ipsum dolor sit amet, itor. Etiam magna lacus, tempor pendiringilla ac ut urna', 'Front-end', 'Beginner'),
('Java Course 1', 'Etiam magna lacus, tempor eget feugiat sed, euismod consectetur lorem. Susper fringilla ac ut urna', 'Fundamentals', 'Beginner'),
('React course', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum non euismod orci, vel aliquam dolor. Quisque mauris nisi, vuiat sed.', 'Computer Science', 'Expert'),
('Algorithms I', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum non euismod orci, vel alit ullamcorper porttitor, commodo sed erat. Tempor eget feugiat sed, euismod consectetur lorem. Suspendisse id tellus ac dolor porttitor fringilla ac ut urna', 'Computer Science', 'Expert'),
('Python', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. vel aliquam dolor. Quisque mauris nisi, vulputate sit amet ullamcorper porttitor. Suspendisse id tellus ac dolor porttitor fringilla ac ut urna', 'Fundamentals', 'Beginner'),
('Java Course 3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum non euismod orci', 'Fundamentals', 'Expert'),
('React course 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum non euismod orci, vel aliquam dolor. Quisque mauris nisi, vuiat sed.', 'Computer Science', 'Expert'),
('Algorithms II', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum non euismod orci, vel alit ullamcorper porttitor, commodo sed erat. Tempor eget feugiat sed, euismod consectetur lorem. Suspendisse id tellus ac dolor porttitor fringilla ac ut urna', 'Computer Science', 'Expert'),
('Python 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. vel aliquam dolor. Quisque mauris nisi, vulputate sit amet ullamcorper porttitor. Suspendisse id tellus ac dolor porttitor fringilla ac ut urna', 'Fundamentals', 'Beginner'),
('Java Course 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum non euismod orci', 'Fundamentals', 'Intermediate');

DROP TABLE IF EXISTS CourseRelation;
CREATE TABLE CourseRelation (
	CourseName nvarchar(50),
	RelatedCourse nvarchar(50),
	PRIMARY KEY(CourseName, RelatedCourse),
	FOREIGN KEY (CourseName) REFERENCES Course (CourseName)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION,
	FOREIGN KEY (RelatedCourse) REFERENCES Course (CourseName)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION
);

DROP TABLE IF EXISTS ContentItem;
CREATE TABLE ContentItem(
	ContentID int PRIMARY KEY,
	Description nvarchar(500),
	PublicationDate date DEFAULT GETDATE(),
	Status nvarchar(30) CHECK (Status IN('Concept', 'Archief', 'Gearchiveerd')),
);

INSERT INTO ContentItem (ContentID, Description, PublicationDate, Status) VALUES 
(1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sagittis orci a scelerisque purus semper eget duis at. In nibh mauris cursus mattis molestie a iaculis at.', GETDATE(), 'Concept'),
(2, 'Quisque non tellus orci ac auctor augue mauris. Lectus vestibulum mattis ullamcorper velit sed ullamcorper. Nunc non blandit massa enim nec dui nunc mattis.', GETDATE(), 'Archief'),
(3, 'Faucibus a pellentesque sit amet. Nisl nisi scelerisque eu ultrices vitae auctor eu augue. Tortor pretium viverra suspendisse potenti nullam ac tortor.', GETDATE(), 'Concept'),
(4, 'Faucibus a pellentesque sit amet. Nisl nisi scelerisque eu ultrices vitae auctor eu augue. Tortor pretium viverra suspendisse potenti nullam ac tortor.', GETDATE(), 'Concept'),
(5, 'Faucibus a pellentesque sit amet. Nisl nisi scelerisque eu ultrices vitae auctor eu augue. Tortor pretium viverra suspendisse potenti nullam ac tortor.', GETDATE(), 'Concept'),
(6, 'Faucibus a pellentesque sit amet. Nisl nisi scelerisque eu ultrices vitae auctor eu augue. Tortor pretium viverra suspendisse potenti nullam ac tortor.', GETDATE(), 'Concept'),
(7, 'Faucibus a pellentesque sit amet. Nisl nisi scelerisque eu ultrices vitae auctor eu augue. Tortor pretium viverra suspendisse potenti nullam ac tortor.', GETDATE(), 'Concept'),
(8, 'Faucibus a pellentesque sit amet. Nisl nisi scelerisque eu ultrices vitae auctor eu augue. Tortor pretium viverra suspendisse potenti nullam ac tortor.', GETDATE(), 'Concept'),
(9, 'Faucibus a pellentesque sit amet. Nisl nisi scelerisque eu ultrices vitae auctor eu augue. Tortor pretium viverra suspendisse potenti nullam ac tortor.', GETDATE(), 'Concept'),
(10, 'Faucibus a pellentesque sit amet. Nisl nisi scelerisque eu ultrices vitae auctor eu augue. Tortor pretium viverra suspendisse potenti nullam ac tortor.', GETDATE(), 'Concept'),
(11, 'Faucibus a pellentesque sit amet. Nisl nisi scelerisque eu ultrices vitae auctor eu augue. Tortor pretium viverra suspendisse potenti nullam ac tortor.', GETDATE(), 'Concept');


DROP TABLE IF EXISTS Lecturer;
CREATE TABLE Lecturer(
	Email nvarchar(50) PRIMARY KEY,
	Name nvarchar(50),
	Organisation nvarchar(50)
);

INSERT INTO Lecturer (Email, Name, Organisation) VALUES
('gert@gmail.com', 'Gerel van Gastel', 'ROC'),
('barbra@gmail.com', 'Barbretta de bab', 'Bart smit'),
('Joep@gmail.com', 'Jopep van den Large', 'Gers pardoel');

DROP TABLE IF EXISTS Webcast;
CREATE TABLE Webcast(
	ContentID int PRIMARY KEY,
	Title nvarchar(50),
	Duration int,
	URL nvarchar(100),
	LecturerEmail nvarchar(50) NOT NULL,
	FOREIGN KEY (ContentID) REFERENCES ContentItem (ContentID)
	ON DELETE CASCADE
	ON UPDATE CASCADE,
	FOREIGN KEY (LecturerEmail) REFERENCES Lecturer (Email)
	ON DELETE NO ACTION
	ON UPDATE CASCADE
);

DROP TABLE IF EXISTS Expert;
CREATE TABLE Expert(
	Email nvarchar(50) PRIMARY KEY,
	Name nvarchar(50),
	Subject nvarchar(50)
);

INSERT INTO Expert (Email, Name, Subject) VALUES
('gert@gmail.com', 'Gerel van Gastel', 'Programming'),
('barbra@gmail.com', 'Barbretta de bab', 'Programming 2'),
('Joep@gmail.com', 'Jopep van den Large', 'Programming 3');


DROP TABLE IF EXISTS ContactPerson;
CREATE TABLE ContactPerson(
	Email nvarchar(50) PRIMARY KEY,
	Name nvarchar(50)
);

INSERT INTO ContactPerson (Email, Name) VALUES
('gert@gmail.com', 'Gerel van Gastel'),
('barbra@gmail.com', 'Barbretta de bab'),
('Joep@gmail.com', 'Jopep van den Large');

DROP TABLE IF EXISTS Module;
CREATE TABLE Module(
	ContentID int PRIMARY KEY,
	Title nvarchar(50),
	Version decimal(4,2),
	IndexNumber int,
	CourseName nvarchar(50) NULL,
	ContactEmail nvarchar(50) NULL,
	FOREIGN KEY (CourseName) REFERENCES Course (CourseName)
	ON DELETE SET NULL
	ON UPDATE CASCADE,
	FOREIGN KEY (ContactEmail) REFERENCES ContactPerson (Email)
	ON DELETE NO ACTION
	ON UPDATE CASCADE,
	FOREIGN KEY (ContentID) REFERENCES ContentItem(ContentID)
	ON DELETE CASCADE
	ON UPDATE CASCADE,
	UNIQUE(Title, Version)
);

INSERT INTO Module (ContentID, Title, Version, IndexNumber, CourseName, ContactEmail) VALUES
(1, 'Java if statements', 1.0, 2, 'Java Course 1', NULL),
(2, 'Java basic syntax', 1.0, 1, 'Java Course 1', NULL),
(3, 'React introduction', 1.0, 1, 'React course 2', NULL),
(4, 'Python basic syntax', 1.0, 1, 'Python', NULL),
(5, 'Algorithms introduction', 1.0, 1, 'Algorithms I', NULL),
(6, 'Algorithms usages', 1.0, 2, 'Algorithms I', NULL),
(7, 'Before we start...', 1.0, 3, 'Algorithms I', NULL),
(8, 'A quick rehersal', 1.0, 1, 'Algorithms II', NULL),
(9, 'Introductory', 1.0, 2, 'Algorithms II', NULL),
(10, 'What have we learnt so far?', 1.0, 1, 'Java Course 2', NULL),
(11, 'Becoming an expert', 1.0, 1, 'Java Course 3', NULL)

DROP TABLE IF EXISTS Student;
CREATE TABLE Student(
	Email nvarchar(50) PRIMARY KEY,
	Name nvarchar(50),
	DateOfBirth date,
	Gender nvarchar(10) CHECK (Gender IN ('male', 'female', 'other')),
	City nvarchar(30),
	Country nvarchar(40),
	Street nvarchar(30),
	HouseNumber int,
	HouseNumberAddendom char(2) NULL,
	Zipcode nvarchar(6)
);

INSERT INTO STUDENT (Email, Name, DateOfBirth, Gender, City, Country, Street, HouseNumber, HouseNumberAddendom, Zipcode) VALUES
('gert@gmail.com', 'Gert van Gastel', '2002-02-12', 'male', 'Breda', 'Nederland', 'VerseStraat', 16, '', '2412LM'),
('barbra@gmail.com', 'Barbera de leeuw', '2002-02-12', 'female', 'Amsterdam', 'Nederland', 'Dooiestraat', 21, '', '3441BM'),
('pieter@gmail.com', 'Pieter el Patron', '2002-02-12', 'male', 'Amsterdam', 'Nederland', 'Bastra', 21, '', '9999LS'),
('Joep@gmail.com', 'Joep van den Berg', '1995-06-12', 'other', 'Leeuwarden', 'Nederland', 'dooiestad', 69, '', '3412QR');

DROP TABLE IF EXISTS Progress;
CREATE TABLE Progress(
	StudentEmail nvarchar(50),
	ContentID int,
	Percentage decimal(5,2) CHECK (Percentage >= 0 AND Percentage <= 100),
	PRIMARY KEY (StudentEmail, ContentID),
	FOREIGN KEY (StudentEmail) REFERENCES Student (Email)
	ON DELETE CASCADE
	ON UPDATE CASCADE,
	FOREIGN KEY (ContentID) REFERENCES ContentItem (ContentID)
	ON DELETE CASCADE
	ON UPDATE CASCADE
);

INSERT INTO Progress (StudentEmail, ContentID, Percentage) VALUES 
('gert@gmail.com', 1, 70),
('pieter@gmail.com', 10, 80),
('gert@gmail.com', 2, 50),
('barbra@gmail.com', 1, 55),
('barbra@gmail.com', 3, 55),
('barbra@gmail.com', 4, 55),
('barbra@gmail.com', 5, 55),
('barbra@gmail.com', 6, 55),
('barbra@gmail.com', 7, 55),
('barbra@gmail.com', 8, 90),
('barbra@gmail.com', 9, 100),
('barbra@gmail.com', 11, 100)

DROP TABLE IF EXISTS Certificate;
CREATE TABLE Certificate(
	CertificateID int IDENTITY(1,1) PRIMARY KEY,
	Grade decimal(4,2) CHECK (Grade >= 0 AND Grade <= 10),
	NameOfExpert nvarchar(40)
);

INSERT INTO Certificate (Grade, NameOfExpert) VALUES 
(5.0, 'Donald Trump'),
(6.7, 'Joe Biden'),
(7.8, 'Ghandi'),
(1.0, 'Bob'),
(2.0, 'Gertje');

DROP TABLE IF EXISTS Registration;
CREATE TABLE Registration(
	RegisterDate date,
	StudentEmail nvarchar(50),
	CourseName nvarchar(50),
	CertificateID int,
	PRIMARY KEY (RegisterDate, StudentEmail, CourseName),
	FOREIGN KEY (CourseName) REFERENCES Course (CourseName)
	ON DELETE NO ACTION
	ON UPDATE CASCADE,
	FOREIGN KEY (CertificateID) REFERENCES Certificate (CertificateID)
	ON DELETE NO ACTION
	ON UPDATE CASCADE,
	FOREIGN KEY (StudentEmail) REFERENCES Student (Email)
	ON DELETE CASCADE
	ON UPDATE CASCADE
);	

INSERT INTO Registration(RegisterDate, StudentEmail, CourseName, CertificateID) VALUES
('20211221', 'gert@gmail.com', 'Java Course 1', 1),
('20211222', 'pieter@gmail.com', 'Java Course 2', 2),
('20200614', 'barbra@gmail.com', 'Algorithms I', 3),
('20200121', 'Joep@gmail.com', 'Python', 4),
('20200122', 'pieter@gmail.com', 'Python', 5);