### INSTALLATIE HANDLEIDING

### Stap 1: Clone de repository
1. Ga naar de gitlab repository: https://gitlab.com/p3163/programmeren-p2.
2. Klik rechtsboven op de blauwe knop 'Clone'.
3. Kopieer SSH of HTTPS.
4. Run de command: git clone {{SSH OF HTTPS}}.

### Stap 2: Toevoegen referenced libraries (links onderin bij Java Projects)
1. Voeg alle .jar files te vinden in setup/fontawesome toe aan de Referenced Libraries.
2. Voeg alle javaFX .jar files toe aan de Referenced libraries.
3. Voeg de mmsql-jdbc-8.4.1.jre11.jar file toe aan de Referenced Libraries.
4. Voeg de jar file in setup/junit toe aan de Referenced Libraries voor de unittesten.

### Step 3: Aanpassen launch.json
1. Voeg de volgende regel toe aan de launch.json (met eigen directory):
"vmArgs": "--module-path \"C:/Users/Rick van Loon/Documents/openjfx-17.0.1_windows-x64_bin-sdk/javafx-sdk-17.0.1/lib\" --add-modules javafx.controls,javafx.fxml".

### Step 4: Database
1. Draai je MSSQL Server.
2. Voer het script uit te vinden in setup/DB2/dbcreate.sql om de database en de tabellen aan te maken met bestaande waardes.

### Step 5: Applicatie runnen
1. Voer het bestand CodeAcademyApplication.java uit te vinden in src/ui.

Programmeren 2 Groepsopdracht
https://brightspace.avans.nl/d2l/lms/group/user_group_list.d2l?ou=28295